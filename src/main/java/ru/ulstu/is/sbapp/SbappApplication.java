package ru.ulstu.is.sbapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Random;

@SpringBootApplication
@RestController
public class SbappApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbappApplication.class, args);
	}

	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hello %s!", name);
	}
	@GetMapping("/mult")
	public int mult(
			@RequestParam(value = "a") int a,
			@RequestParam(value = "b") int b
	) {
		return a + b;
	}
	@GetMapping("/task")
	public int[][] task(
			@RequestParam(value = "num1") int num1,
			@RequestParam(value = "num2") int num2
	) {
		int[][] a = new int[num1][num2];
		Random random = new Random();
		for (int i = 0; i < num1; i++) {
			for (int j = 0; j < num2; j++) {
				a[i][j] = random.nextInt(100);
			}
		}
		return a;
	}
	@GetMapping("/task1")
	public int task2(
			@RequestParam(value = "num1", defaultValue = "5") int a,
			@RequestParam(value = "num2") int b
	) {

		return a-b;
	}


}

